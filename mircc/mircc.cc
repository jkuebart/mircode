#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "mircode/generator/cxx.h"
#include "mircode/parser/parser.h"

namespace {
    void convert(std::ostream& os, std::istream& in) {
        auto const res = mircode::parser::parse(in);
        if (res.first) {
            mircode::generator::cxx::generate(os, res.second);
        }
    }
}

int Main(std::vector<std::string> args) {
    args.erase(args.begin());
    if (args.empty()) {
        args.emplace_back("-");
    }

    for (auto const& arg : args) {
        if ("-" == arg) {
            convert(std::cout, std::cin);
        } else {
            std::ifstream fin(arg);
            if (!fin) {
                std::cerr << arg << ": open failed" << std::endl;
                return 1;
            }
            convert(std::cout, fin);
        }
    }

    return 0;
}

int
main(int
c,char**v){return
Main(std::vector<std::string>(v,c+v));}
