#include "mircode/parser/parser.h"

#include <iostream>
#include <string>
#include <vector>

#include <boost/fusion/adapted.hpp>
#include <boost/spirit/home/x3.hpp>
#include <boost/spirit/include/support_multi_pass.hpp>

#include "mircode/ast/ast.h"

namespace x3 = boost::spirit::x3;

using x3::alnum;
using x3::alpha;
using x3::char_;
using x3::int_;
using x3::lexeme;
using x3::lit;
using x3::rule;
using x3::string;

BOOST_FUSION_ADAPT_STRUCT(
    mircode::ast::decl,
    (std::string, type)
    (std::string, id)
)

BOOST_FUSION_ADAPT_STRUCT(
    mircode::ast::fun_def,
    (std::string, name)
    (std::vector<mircode::ast::decl>, args)
    (std::vector<mircode::ast::decl>, results)
    (std::vector<mircode::ast::stmt>, stmts)
)

BOOST_FUSION_ADAPT_STRUCT(
    mircode::ast::stmt_return,
    (mircode::ast::expr, result)
)

BOOST_FUSION_ADAPT_STRUCT(
    mircode::ast::struct_literal,
    (std::vector<mircode::ast::expr>, values)
)

namespace mircode {
    namespace parser {
        namespace {
            auto const number = int_;
            auto const quoted_string = lexeme['"' >> *(char_ - '"') >> '"'];

            auto const type =
                  string("bool")
                | string("double")
                | string("float")
                | string("int")
                ;

            rule<struct decl, ast::decl> const decl = "declaration";
            rule<struct expr, ast::expr> const expr = "expression";
            rule<struct fun_def, ast::fun_def> const fun_def = "function-definition";
            rule<struct id, std::string> const id = "identifier";
            rule<struct program, std::vector<ast::stmt>> const program = "program";
            rule<struct stmt, ast::stmt> const stmt = "statement";
            rule<struct stmt_return, ast::stmt_return> const stmt_return = "return-statement";
            rule<struct struct_literal, ast::struct_literal> const struct_literal = "struct-literal";

            auto const decl_def =
                type >> id;
            BOOST_SPIRIT_DEFINE(decl)

            auto const expr_def =
                  number
                | quoted_string
                | struct_literal
                ;
            BOOST_SPIRIT_DEFINE(expr)

            auto const fun_def_def =
                lit("fun") >> id
                >> -(lit("maps") >> '(' >> (decl % ',') >> ')')
                >> -(lit("to") >> '[' >> (decl % ';') >> ']')
                >> "sothat" >> *(stmt >> ';') >> "endf"
                ;
            BOOST_SPIRIT_DEFINE(fun_def)

            auto const id_def = lexeme[alpha >> *(alnum)];
            BOOST_SPIRIT_DEFINE(id)

            auto const program_def =
                *stmt
                ;
            BOOST_SPIRIT_DEFINE(program)

            auto const stmt_def =
                  fun_def
                | stmt_return
                ;
            BOOST_SPIRIT_DEFINE(stmt)

            auto const stmt_return_def =
                "return" >> expr;
            BOOST_SPIRIT_DEFINE(stmt_return)

            auto const struct_literal_def =
                '{' >> (expr % ',') >> '}';
            BOOST_SPIRIT_DEFINE(struct_literal)

            template<typename Iterator>
            std::pair<bool, std::vector<ast::stmt>>
            parse(Iterator first, Iterator const& last) {
                std::vector<ast::stmt> program;
                bool res = x3::phrase_parse(first, last, mircode::parser::program, x3::space, program);
                res &= (first == last);

                if (!res) {
                    std::cerr << "syntax error at: " << std::string(first, last) << std::endl;
                }
                return std::make_pair(res, program);
            }
        }

        std::pair<bool, std::vector<ast::stmt>>
        parse(std::string const& str) {
            return parse(str.begin(), str.end());
        }

        std::pair<bool, std::vector<ast::stmt>>
        parse(std::istream& in) {
            in >> std::noskipws;

            using iterator_type = std::istreambuf_iterator<char>;
            auto const first = boost::spirit::make_default_multi_pass(iterator_type(in));
            auto const last = boost::spirit::make_default_multi_pass(iterator_type());

            return parse(first, last);
        }
    }
}
