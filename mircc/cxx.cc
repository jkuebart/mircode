#include "mircode/generator/cxx.h"

#include <ostream>
#include <string>

#include "mircode/ast/ast.h"

namespace mircode {
    namespace generator {
        namespace cxx {
            namespace {
                class generator {
                public:
                    explicit generator(std::ostream& os)
                    : m_os(os)
                    {}

                    void generate(ast::stmt const& stmt) const {
                        boost::apply_visitor(*this, stmt);
                        m_os << std::endl;
                    }

                    void operator()(int i) const {
                        m_os << i;
                    }

                    void operator()(std::string const& str) const {
                        m_os << '"' << str << '"';
                    }

                    void operator()(ast::fun_def const& fun) const {
                        if (fun.results.empty()) {
                            m_os << "void ";
                        } else {
                            m_os <<
                                "struct {" << std::endl;
                            generate_def(fun.results);
                            m_os <<
                                "}" << std::endl;
                        }

                        m_os <<
                            fun.name << '(';
                        generate_decl(fun.args);
                        m_os <<
                            ')' << std::endl <<
                            '{' << std::endl;
                        for (auto const& stmt : fun.stmts) {
                            generate(stmt);
                        }
                        m_os << '}';
                    }

                    void operator()(ast::stmt_return const& ret) const {
                        m_os << "return ";
                        generate(ret.result);
                        m_os << ';';
                    }

                    void operator()(ast::struct_literal const& struct_lit) const {
                        m_os << '{';
                        generate_seq(struct_lit.values, ", ");
                        m_os << '}';
                    }

                private:
                    template<typename Range>
                    void generate_seq(Range const& range, std::string const& sep) const {
                        using std::begin;
                        using std::end;
                        auto const first = begin(range);
                        for (auto it = first; it != end(range); ++it) {
                            if (first != it) {
                                m_os << sep;
                            }
                            generate(*it);
                        }
                    }

                    void generate_decl(std::vector<ast::decl> const& decls) const {
                        return generate_seq(decls, ", ");
                    }

                    void generate_def(std::vector<ast::decl> const& decls) const {
                        for (auto const& decl : decls) {
                            generate(decl);
                            m_os << ';' << std::endl;
                        }
                    }

                    void generate(ast::decl const& decl) const {
                        m_os << decl.type << ' ' << decl.id;
                    }

                    void generate(ast::expr const& expr) const {
                        boost::apply_visitor(*this, expr);
                    }

                private:
                    std::ostream& m_os;
                };
            }

            void generate(std::ostream& os, ast::stmt const& stmt) {
                return generator(os).generate(stmt);
            }

            void generate(std::ostream& os, std::vector<ast::stmt> const& stmts) {
                for (auto const& stmt : stmts) {
                    generate(os, stmt);
                }
            }
        }
    }
}
