#ifndef MIRCODE_AST_H
#define MIRCODE_AST_H

#include <string>
#include <vector>

#include <boost/spirit/home/x3/support/ast/variant.hpp>

namespace mircode {
    namespace ast {
        struct decl {
            std::string type;
            std::string id;
        };

        struct struct_literal;
        struct expr: boost::spirit::x3::variant<
            int,
            std::string,
            boost::spirit::x3::forward_ast<struct_literal>
        >
        {
            using base_type::base_type;
            using base_type::operator =;
        };

        struct struct_literal {
            std::vector<expr> values;
        };

        struct stmt;
        struct fun_def {
            std::string name;
            std::vector<decl> args;
            std::vector<decl> results;
            std::vector<stmt> stmts;
        };

        struct stmt_return {
            expr result;
        };

        struct stmt: boost::spirit::x3::variant<
            fun_def,
            stmt_return
        >
        {
            using base_type::base_type;
            using base_type::operator =;
        };
    }
}

#endif // MIRCODE_AST_H
