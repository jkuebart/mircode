#ifndef MIRCODE_PARSER_PARSER_H
#define MIRCODE_PARSER_PARSER_H

#include <istream>
#include <string>
#include <vector>

#include "mircode/ast/ast.h"

namespace mircode {
    namespace parser {
        /**
         * Parse the given string.
         *
         * @param str The string to parse.
         * @return The first item indicates success. The second item
         *         contains the result of a successful parse.
         */
        std::pair<bool, std::vector<ast::stmt>> parse(std::string const& str);

        /**
         * Parse the given stream.
         *
         * @param in The stream to parse.
         * @return The first item indicates success. The second item
         *         contains the result of a successful parse.
         */
        std::pair<bool, std::vector<ast::stmt>> parse(std::istream& in);
    }
}

#endif // MIRCODE_PARSER_PARSER_H
