#ifndef MIRCODE_GENERATOR_CXX
#define MIRCODE_GENERATOR_CXX

#include <ostream>
#include <vector>

#include "mircode/ast/ast.h"

namespace mircode {
    namespace generator {
        namespace cxx {
            /**
             * Generate C++ code for a statement.
             *
             * @param os An output stream.
             * @param stmt A statement.
             */
            void generate(std::ostream& os, ast::stmt const& stmt);

            /**
             * Generate C++ code for statements.
             *
             * @param os An output stream.
             * @param stmts A sequence of statemets.
             */
            void generate(std::ostream& os, std::vector<ast::stmt> const& stmts);
        }
    }
}

#endif // MIRCODE_GENERATOR_CXX
